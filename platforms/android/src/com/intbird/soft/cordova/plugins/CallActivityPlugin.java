package com.intbird.soft.cordova.plugins;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by intbird on 15/6/11.
 */
public class CallActivityPlugin extends CordovaPlugin {

    public static final String  ACTION = "call";

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if(this.cordova.getActivity().isFinishing()) return true;

        if(action.equals(ACTION)){
            try{
                Intent intent = new Intent(cordova.getActivity(),Class.forName(args.getString(0)));
                this.cordova.startActivityForResult(this,intent,-1);

                PluginResult mPlugin = new PluginResult(PluginResult.Status.NO_RESULT);
                mPlugin.setKeepCallback(true);
                callbackContext.sendPluginResult(mPlugin);

                callbackContext.success("activity started.");
                callbackContext.error("activity not start.");

            }catch(Exception ex){
                ex.printStackTrace();
                return  false;
            }
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        switch (resultCode){
            case Activity.RESULT_OK:
                Bundle b = intent.getExtras();
                String str = b.getString("changge01");
                break;
            default:
                break;
        }
    }
}
